var mongoose = require('../config/database');
var Schema = mongoose.Schema;

var ExpenseSchema = new Schema({
    description: {
        type: Schema.Types.String,
        required: true, 
    },
    amount:{
        type: Schema.Types.Number,
        required: true,
    },
    date:{
        type: Schema.Types.Date,
        required: true,
    },
},{timestemps: true});

module.exports = mongoose.model('Expense', ExpenseSchema);