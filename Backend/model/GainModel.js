var mongoose = require('../config/database');
var Schema = mongoose.Schema;

var User = require('./UserModel');

var GainSchema = new Schema({
    /*owner: {
        type: User,
        required: true,
    },*/
    description: {
        type: Schema.Types.String,
        required: true, 
    },
    amount:{
        type: Schema.Types.Number,
        required: true,
    },
    date:{
        type: Schema.Types.Date,
        required: true,
    },
},{timestemps: true});

module.exports = mongoose.model('Gain', GainSchema);