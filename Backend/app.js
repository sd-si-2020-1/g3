var { initialize } = require('express-openapi');
var v1ApiDoc = require('./apiDoc/api-doc');

var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');



var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var GainRouter = require('./routes/GainRoutes');
var ExpenseRouter = require('./routes/ExpenseRoutes');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('./routes'));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use('/gain', GainRouter);

app.use('/expense', ExpenseRouter);

initialize({
    app,
    apiDoc: v1ApiDoc,
    paths: './apiDoc/paths'
  });

module.exports = app;
