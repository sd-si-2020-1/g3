var express = require('express');
var router = express.Router();

var ExpenseController = require('../controller/ExpenseController');
var ExpenseValidation = require('../middlewares/ExpenseValidation');

router.post('/', ExpenseValidation, ExpenseController.create);
router.put('/:id', ExpenseController.update);

module.exports = router;