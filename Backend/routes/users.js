var express = require('express');
var router = express.Router();
var UserController = require('../controller/UserController');
var UserValidation = require('../middlewares/UserValidation');


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/', UserValidation,  UserController.create);

module.exports = router;
