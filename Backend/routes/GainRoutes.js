var express = require('express');
var router = express.Router();

var GainController = require('../controller/GainController');
var GainValidation = require('../middlewares/GainValidation');

router.post('/', GainValidation, GainController.create);
router.put('/:id', GainController.update);

router.get('/filter/all', GainController.all);
router.get('/:id', GainController.show);

router.delete('/:id', GainController.delete);

module.exports = router;