//Import the mongoose module
var mongoose = require('mongoose');

//Set up default mongoose connection
var mongoDB = 'mongodb://localhost:27017/carteira_online';
mongoose.connect(mongoDB, { useNewUrlParser: true });

//Get the default connection
module.exports = mongoose;
