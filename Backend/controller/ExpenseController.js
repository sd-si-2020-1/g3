const { response } = require('express');
var ExpenseModel = require('../model/ExpenseModel');

class ExpenseController{

    async create(req, res){
        var expense = new ExpenseModel(req.body);
        await expense
                .save()
                .then(response => {
                    return res.status(200).json(response);
                })
                .catch(error => {
                    return res.status(500).json(error);
                })
    }
   
    async update(req, res){
        await ExpenseModel.findByIdAndUpdate({'_id': req.params.id}, req.body, {new: true})
        .then(response => {
            return res.status(200).json(response);
        })
        .catch(error => {
            return res.status(500).json(error);
        });
    }

    async show(req, res){
        await ExpenseModel,findById(req.params.id)
        .then(response => {
            if(response)
                return res.status(200).json(response);
            else
                return res.status(404).json({error: 'Não encontrado'});
        })
        .catch(error => {
            return res.status(500).json(error);
        });
    }
}

module.exports = new ExpenseController();