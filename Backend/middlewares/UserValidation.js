var UserModel = require("../model/UserModel");

var UserValidation = async (req, res, next) => {

    var { firstName, lastName, email, username, password } = req.body;

    if(!firstName)
    return res.status(400).json({ error: "Primeiro nome é obrigatório"});
    else if(!lastName)
    return res.status(400).json({ error: "Segundo nome é obrigatório"});
    else if(!email)
    return res.status(400).json({ error: "Email é obrigatório"});
    else if(!username)
    return res.status(400).json({ error: "Usuário é obrigatório"});
    else if(!password)
    return res.status(400).json({ error: "Senha é obrigatório"});
    else{
        let existUser, existEmail;

        existUser = await UserModel.findOne({'username': username});
        
        existEmail = await UserModel.findOne({'email': email});

        if(existUser){
            return res.status(400).json({ error: "Usuário já utilizado"});
        }else if(existEmail){
            return res.status(400).json({ error: "Email já utilizado"});
        }

        next();
    }
    
    
}

module.exports = UserValidation;