var ExpenseModel = require("../model/ExpenseModel");
var { isFuture } = require('date-fns');

var ExpenseValidation = async (req, res, next) => {

    var { description, amount, date } = req.body;

    if(!description)
    return res.status(400).json({ error: "Descrição é obrigatório"});
    else if(!amount)
    return res.status(400).json({ error: "Valor é obrigatório"});
    else if(!date)
    return res.status(400).json({ error: "Data é obrigatório"});
    else if(isFuture(new Date(date)))
    return res.status(400).json({ error: "Escolha uma data atual ou retroativa"});
    else
    next();
    
}

module.exports = ExpenseValidation;